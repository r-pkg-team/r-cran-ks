Source: r-cran-ks
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Section: gnu-r
Testsuite: autopkgtest-pkg-r
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-cran-fnn,
               r-cran-kernlab,
               r-cran-kernsmooth,
               r-cran-matrix,
               r-cran-mclust,
               r-cran-mgcv,
               r-cran-multicool,
               r-cran-mvtnorm,
               r-cran-pracma
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-ks
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-ks.git
Homepage: https://cran.r-project.org/package=ks
Rules-Requires-Root: no

Package: r-cran-ks
Architecture: any
Depends: ${R:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GNU R kernel smoothing
 Kernel smoothers for univariate and multivariate data, including
 densities, density derivatives, cumulative distributions, clustering,
 classification, density ridges, significant modal regions, and two-
 sample hypothesis tests. Chacon & Duong (2018)
 <doi:10.1201/9780429485572>.
